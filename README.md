## Version
    php 7.3
    laravel 8x
    composer 2.1.11

## Copy environment
    cp .env.example .env

## Docker build
    docker-compose build

## Docker run
    docker-compose up -d

#   Init project
## Install framework laravel 8
    docker exec -it core bash
    composer create-project laravel/laravel:8.* .

## Package recomment
#### Using optimized uuid
    dyrynda/laravel-efficient-uuid
    dyrynda/laravel-model-uuid

#### Using JWT Token
    tymon/jwt-auth

#### Using cache redis
    predis/predis
