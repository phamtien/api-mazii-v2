# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 159.89.203.36 (MySQL 5.7.33-0ubuntu0.16.04.1-log)
# Database: admin_api
# Generation Time: 2022-05-23 01:28:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table active_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `active_user`;

CREATE TABLE `active_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_check_word
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_check_word`;

CREATE TABLE `admin_check_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_admin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_word_id` int(11) NOT NULL,
  `new_word_status` tinyint(4) NOT NULL,
  `old_word` text COLLATE utf8_unicode_ci NOT NULL,
  `new_word` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_community
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_community`;

CREATE TABLE `admin_community` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `word_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `document` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_get_code_jlpt
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_get_code_jlpt`;

CREATE TABLE `admin_get_code_jlpt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_gram_review
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_gram_review`;

CREATE TABLE `admin_gram_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `log` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_grammar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_grammar`;

CREATE TABLE `admin_grammar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_admin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grammar_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `grammar_status` tinyint(4) NOT NULL,
  `old_grammar` text COLLATE utf8_unicode_ci NOT NULL,
  `new_grammar` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_job
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_job`;

CREATE TABLE `admin_job` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `oid` varchar(64) DEFAULT NULL COMMENT 'origin id in other site',
  `olink` varchar(1024) DEFAULT NULL COMMENT 'origin link in other site',
  `title` text,
  `salary_begin` bigint(20) DEFAULT NULL,
  `salary_end` bigint(20) DEFAULT NULL,
  `currency` varchar(32) DEFAULT NULL,
  `apply_link` varchar(1024) NOT NULL,
  `city` varchar(8) DEFAULT NULL,
  `country` varchar(8) NOT NULL,
  `expire_date` date NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table admin_kanji
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_kanji`;

CREATE TABLE `admin_kanji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_admin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kanji_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kanji_status` tinyint(4) NOT NULL,
  `old_kanji` text COLLATE utf8_unicode_ci NOT NULL,
  `new_kanji` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_notify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_notify`;

CREATE TABLE `admin_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table admin_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_report`;

CREATE TABLE `admin_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_admin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `report_id` int(30) NOT NULL,
  `word_status` tinyint(4) NOT NULL,
  `mean_old` text COLLATE utf8_unicode_ci NOT NULL,
  `mean_new` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `type_admin` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin' COMMENT 'admin: admin thuộc trang admin, admin2: admin thuộc trang admin2',
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_voca
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_voca`;

CREATE TABLE `admin_voca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_admin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voca_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `voca_status` tinyint(4) NOT NULL,
  `old_voca` text COLLATE utf8_unicode_ci NOT NULL,
  `new_voca` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table admin_word_review
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_word_review`;

CREATE TABLE `admin_word_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `word_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `log` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `review_times` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table app_review_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_review_actions`;

CREATE TABLE `app_review_actions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `app_review_id` int(11) NOT NULL,
  `action` tinyint(1) DEFAULT NULL COMMENT '1: like, 0: dislike',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table app_reviews
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_reviews`;

CREATE TABLE `app_reviews` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `star` tinyint(1) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ios or android',
  `likes` int(11) NOT NULL DEFAULT '0',
  `dislikes` int(11) NOT NULL DEFAULT '0',
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `categoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryName` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  `parent_cate_id` int(11) DEFAULT '0',
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `share_status` tinyint(1) NOT NULL DEFAULT '0',
  `share_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryId`),
  KEY `userId` (`userId`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`),
  KEY `share_hash` (`share_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table com_means
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_means`;

CREATE TABLE `com_means` (
  `means_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_id` int(11) NOT NULL,
  `mean1` int(11) NOT NULL,
  `mean2` int(11) NOT NULL,
  `mean3` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`means_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table com_news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_news`;

CREATE TABLE `com_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news` longtext COLLATE utf8_unicode_ci NOT NULL,
  `news_mean` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table com_points
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_points`;

CREATE TABLE `com_points` (
  `point_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`point_id`),
  KEY `index_userId` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table com_reviewers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_reviewers`;

CREATE TABLE `com_reviewers` (
  `reviewer_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`reviewer_id`),
  KEY `index_userId` (`user_id`),
  KEY `index_nameId` (`name_id`),
  KEY `index_action` (`action`),
  FULLTEXT KEY `index_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table com_sentences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_sentences`;

CREATE TABLE `com_sentences` (
  `sen_id` int(11) NOT NULL AUTO_INCREMENT,
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `sentence` text COLLATE utf8_unicode_ci,
  `transcription` text COLLATE utf8_unicode_ci NOT NULL,
  `sen_mean` text COLLATE utf8_unicode_ci,
  `mean1` int(11) NOT NULL,
  `mean2` int(11) NOT NULL,
  `mean3` int(11) NOT NULL,
  `total` tinyint(4) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `rate` tinyint(4) NOT NULL,
  `mean_right` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_final` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`sen_id`),
  KEY `status` (`status`),
  KEY `index_total` (`total`),
  KEY `index_rate` (`rate`),
  FULLTEXT KEY `index_sentence` (`sentence`),
  FULLTEXT KEY `index_transcription` (`transcription`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table com_translators
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_translators`;

CREATE TABLE `com_translators` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_id` int(11) NOT NULL,
  `mean` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `rate` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`trans_id`),
  KEY `index_nameId` (`name_id`),
  KEY `index_userId` (`user_id`),
  KEY `index_updatedAt` (`updated_at`),
  KEY `index_rate` (`rate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table com_word
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_word`;

CREATE TABLE `com_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL,
  `phonetic` varchar(255) NOT NULL,
  `mean` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `reviewer` tinyint(4) NOT NULL DEFAULT '0',
  `trans_reviewer` tinyint(4) NOT NULL DEFAULT '0',
  `couch_id` varchar(100) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table com_word_rank
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_word_rank`;

CREATE TABLE `com_word_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `number` int(11) NOT NULL,
  `rank` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table com_word_reviewer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_word_reviewer`;

CREATE TABLE `com_word_reviewer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `word_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `review` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1: true; 0: false',
  `answer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table com_word_translate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `com_word_translate`;

CREATE TABLE `com_word_translate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `word_id` int(11) NOT NULL,
  `translate` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: false; 1: true',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table device
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `deviceId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `firebaseToken` text COLLATE utf8_unicode_ci,
  `platform` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'web, ios, android',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `deviceId` (`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table domain_verify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domain_verify`;

CREATE TABLE `domain_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `callback` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1: verify, 0: not verify',
  `logo_path` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table enrollment_register
# ------------------------------------------------------------

DROP TABLE IF EXISTS `enrollment_register`;

CREATE TABLE `enrollment_register` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `enrollment_id` bigint(20) NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opinion` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table enrollment_statistical
# ------------------------------------------------------------

DROP TABLE IF EXISTS `enrollment_statistical`;

CREATE TABLE `enrollment_statistical` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `enrollment_id` bigint(20) NOT NULL,
  `total_requests` int(11) NOT NULL DEFAULT '0',
  `total_clicks` int(11) NOT NULL DEFAULT '0',
  `total_applies` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table enrollments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `enrollments`;

CREATE TABLE `enrollments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `requirement` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `study_type_id` bigint(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `avatar` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `benefit` longtext COLLATE utf8_unicode_ci,
  `partner_id` int(11) NOT NULL,
  `tuition` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enrollments_partner_id_index` (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table errors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `errors`;

CREATE TABLE `errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` text NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table feedbacks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedbacks`;

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(200) NOT NULL,
  `favorite` tinyint(1) NOT NULL,
  `content` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flashcard
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flashcard`;

CREATE TABLE `flashcard` (
  `flashcardId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wordId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`flashcardId`),
  KEY `userId` (`userId`),
  KEY `wordId` (`wordId`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table form
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form`;

CREATE TABLE `form` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `contact` text COLLATE utf8_unicode_ci,
  `favorite_contact` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vn',
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `career_status_id` bigint(20) DEFAULT NULL,
  `other_career_status` text COLLATE utf8_unicode_ci,
  `current_degree_id` bigint(20) DEFAULT NULL,
  `other_current_degree` text COLLATE utf8_unicode_ci,
  `visa_expire` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile` text COLLATE utf8_unicode_ci,
  `profile_difficulty` text COLLATE utf8_unicode_ci,
  `job_type_id` bigint(20) DEFAULT NULL,
  `other_job_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_detail` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expected_income` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_time` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_require` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Mới, 1: Đang xử lý, 2: Hoàn thành, -1: Hủy',
  `platform` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table form_career_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form_career_status`;

CREATE TABLE `form_career_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table form_current_degree
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form_current_degree`;

CREATE TABLE `form_current_degree` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table form_job_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form_job_type`;

CREATE TABLE `form_job_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table form_region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form_region`;

CREATE TABLE `form_region` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` bigint(20) NOT NULL,
  `region_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jlpt_base_question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jlpt_base_question`;

CREATE TABLE `jlpt_base_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jlpt_number` tinyint(4) NOT NULL,
  `type_id` int(11) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `read_content_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` tinyint(4) NOT NULL,
  `answer_1` text NOT NULL,
  `answer_2` text NOT NULL,
  `answer_3` text NOT NULL,
  `answer_4` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jlpt_code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jlpt_code`;

CREATE TABLE `jlpt_code` (
  `code_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kind` tinyint(4) NOT NULL COMMENT '0:0; 1:5, 2:10',
  `time` int(6) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0-created; 1-wait; 2-active; -1-delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jlpt_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jlpt_info`;

CREATE TABLE `jlpt_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `shortDes` text COLLATE utf8_unicode_ci NOT NULL,
  `descHtml` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `admin_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '17',
  `view` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jlpt_open_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jlpt_open_test`;

CREATE TABLE `jlpt_open_test` (
  `open_test_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `test_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `premium` tinyint(1) NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `short_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`open_test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jlpt_test_times
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jlpt_test_times`;

CREATE TABLE `jlpt_test_times` (
  `times_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `times` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`times_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jlpt_total_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jlpt_total_test`;

CREATE TABLE `jlpt_total_test` (
  `total_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `total` int(6) NOT NULL,
  `expire_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`total_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jlpt_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jlpt_transaction`;

CREATE TABLE `jlpt_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_applies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_applies`;

CREATE TABLE `job_applies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `job_id` bigint(20) NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opnion` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_majors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_majors`;

CREATE TABLE `job_majors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_permission_role`;

CREATE TABLE `job_permission_role` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `job_permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_permissions`;

CREATE TABLE `job_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_roles`;

CREATE TABLE `job_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_statistical
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_statistical`;

CREATE TABLE `job_statistical` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` bigint(20) NOT NULL,
  `total_requests` int(11) NOT NULL DEFAULT '0',
  `total_clicks` int(11) NOT NULL DEFAULT '0',
  `total_applies` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_user_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_user_permission`;

CREATE TABLE `job_user_permission` (
  `permission_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `job_user_permission_permission_id_index` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table job_user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_user_role`;

CREATE TABLE `job_user_role` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `job_user_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `require` text COLLATE utf8_unicode_ci NOT NULL,
  `benifit` text COLLATE utf8_unicode_ci NOT NULL,
  `salary` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `on_position` varchar(191) COLLATE utf8_unicode_ci DEFAULT '1',
  `type` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `majors` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_pin` tinyint(4) NOT NULL DEFAULT '0',
  `user_pin` tinyint(4) NOT NULL DEFAULT '0',
  `admin_pin_start` datetime DEFAULT NULL,
  `admin_pin_end` datetime DEFAULT NULL,
  `partner_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_partner_id_index` (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jp_region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jp_region`;

CREATE TABLE `jp_region` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table key_reset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `key_reset`;

CREATE TABLE `key_reset` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `short` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `country` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table log_sync
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log_sync`;

CREATE TABLE `log_sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `type` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mazii
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mazii`;

CREATE TABLE `mazii` (
  `maziiId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`maziiId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table mazii_admin_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mazii_admin_role`;

CREATE TABLE `mazii_admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mazii_admin_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mazii_admin_user`;

CREATE TABLE `mazii_admin_user` (
  `id_admin` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `role_id` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table mazii_purchase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mazii_purchase`;

CREATE TABLE `mazii_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `items` varchar(200) NOT NULL,
  `price` int(11) NOT NULL,
  `price_raw` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `care_dairy` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log` text,
  `device` varchar(255) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `time_success` timestamp NULL DEFAULT NULL,
  `level` varchar(10) DEFAULT 'l1',
  `source` varchar(100) DEFAULT NULL,
  `renewal` tinyint(4) DEFAULT NULL COMMENT '0: đơn mua mới, 1: đơn gia hạn',
  `currency` varchar(191) DEFAULT NULL,
  `care_dairy_time` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật care_dairy lần cuối',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table newWord
# ------------------------------------------------------------

DROP TABLE IF EXISTS `newWord`;

CREATE TABLE `newWord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'javi',
  `word` text COLLATE utf8_unicode_ci NOT NULL,
  `phonetic` text COLLATE utf8_unicode_ci,
  `mean` text COLLATE utf8_unicode_ci NOT NULL,
  `example` text COLLATE utf8_unicode_ci,
  `example_mean` text COLLATE utf8_unicode_ci,
  `userId` int(11) NOT NULL,
  `username` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `word_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `review` tinyint(4) NOT NULL COMMENT '1: reviewed',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `index_new_word` (`word`(20)),
  KEY `index_userId` (`userId`),
  KEY `index_status` (`status`),
  KEY `index_wordId` (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table note
# ------------------------------------------------------------

DROP TABLE IF EXISTS `note`;

CREATE TABLE `note` (
  `noteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noteName` text COLLATE utf8_unicode_ci NOT NULL,
  `noteMean` text COLLATE utf8_unicode_ci NOT NULL,
  `cateId` int(11) NOT NULL DEFAULT '0',
  `subCateId` int(11) DEFAULT '0',
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idx` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`noteId`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`),
  KEY `cateId` (`cateId`),
  KEY `subCateId` (`subCateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table note_extend
# ------------------------------------------------------------

DROP TABLE IF EXISTS `note_extend`;

CREATE TABLE `note_extend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `noteId` int(11) DEFAULT NULL,
  `phonetic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `noteId` (`noteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table partner_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partner_type`;

CREATE TABLE `partner_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `delay_form` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table partners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partners`;

CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `website` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `sort_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction` text COLLATE utf8_unicode_ci,
  `tax_code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_type_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:hide, 1:active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table payment_received
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_received`;

CREATE TABLE `payment_received` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secret` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `bank_id` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `time` varchar(50) NOT NULL,
  `order_code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table payment_secrets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_secrets`;

CREATE TABLE `payment_secrets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secret` varchar(12) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table premium
# ------------------------------------------------------------

DROP TABLE IF EXISTS `premium`;

CREATE TABLE `premium` (
  `premiumId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deviceId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  `transaction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expire_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product` text COLLATE utf8_unicode_ci,
  `receipt` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`premiumId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table product_center
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_center`;

CREATE TABLE `product_center` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `profile_name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `level` tinyint(4) DEFAULT NULL,
  `follow` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `info` tinyint(1) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `card_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `card_date` date DEFAULT NULL,
  `card_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `guardian` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `guardian_card` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `facebook` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `birthday` date DEFAULT NULL,
  `sex` tinyint(4) NOT NULL,
  `job` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `website` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `introduction` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `private` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'email-phone-facebook-address-sex-birthday',
  `need` int(6) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `user_id` (`user_id`),
  KEY `profile_name` (`profile_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table profile_needs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `profile_needs`;

CREATE TABLE `profile_needs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ja` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ko` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_zh-CN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_zh-TW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_tl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0:show  , -1: hide',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table promote
# ------------------------------------------------------------

DROP TABLE IF EXISTS `promote`;

CREATE TABLE `promote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `transaction` int(11) NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table purchase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase`;

CREATE TABLE `purchase` (
  `userId` int(10) NOT NULL,
  `transation` text NOT NULL,
  `provider` text NOT NULL,
  `extra` varchar(50) NOT NULL,
  `deviceId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table purchase_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase_history`;

CREATE TABLE `purchase_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_card_key` varchar(25) DEFAULT NULL,
  `playstore_order_id` varchar(100) DEFAULT NULL,
  `serial_key` varchar(25) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(500) NOT NULL,
  `appstore_transaction_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rank
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rank`;

CREATE TABLE `rank` (
  `rank_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `week` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table rate_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rate_report`;

CREATE TABLE `rate_report` (
  `rateId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `reportId` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`rateId`),
  KEY `reportId` (`reportId`),
  KEY `userId` (`userId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table report_check_grammar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report_check_grammar`;

CREATE TABLE `report_check_grammar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL,
  `request_sentences` text COLLATE utf8_unicode_ci NOT NULL,
  `response_sentences` text COLLATE utf8_unicode_ci NOT NULL,
  `feedback` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_check_grammar_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table report_mean
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report_mean`;

CREATE TABLE `report_mean` (
  `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `dict` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'javi',
  `wordId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '-1: deleted; 0: created; 1: edited; 2: save database',
  `like` int(11) NOT NULL,
  `dislike` int(11) NOT NULL,
  `mean` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `word` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `type_data` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'word',
  PRIMARY KEY (`reportId`),
  KEY `userId` (`userId`),
  KEY `wordId` (`wordId`) USING BTREE,
  KEY `dislike` (`dislike`) USING BTREE,
  FULLTEXT KEY `word` (`word`),
  FULLTEXT KEY `type_data` (`type_data`),
  FULLTEXT KEY `dict` (`dict`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table save_partner_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `save_partner_posts`;

CREATE TABLE `save_partner_posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_armorial
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_armorial`;

CREATE TABLE `soc_armorial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ko` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_zh-CN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_zh-TW` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_tl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `point` int(11) NOT NULL COMMENT 'Mức điểm cần đạt',
  `status` tinyint(4) NOT NULL COMMENT '0: show; -1: hide',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_category`;

CREATE TABLE `soc_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ko` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_zh-CN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_zh-TW` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `name_tl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_fr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `follow` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0: show; -1: hide',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_comment`;

CREATE TABLE `soc_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `total_comment` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `liked` int(11) NOT NULL,
  `dislike` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '17',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `word_spam` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_event_realtime
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_event_realtime`;

CREATE TABLE `soc_event_realtime` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_actions` varchar(255) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `target_id` int(11) DEFAULT '0',
  `event_type` varchar(25) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0: chưa xem, 1: đã xem',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table soc_event_timestamp
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_event_timestamp`;

CREATE TABLE `soc_event_timestamp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table soc_follow
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_follow`;

CREATE TABLE `soc_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `follow_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0: active; -1: deactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`follow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_like_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_like_comment`;

CREATE TABLE `soc_like_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `action` tinyint(4) NOT NULL COMMENT '1: like; -1: dislike',
  `language_id` int(11) NOT NULL DEFAULT '17',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_like_par_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_like_par_comment`;

CREATE TABLE `soc_like_par_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `par_comment_id` int(11) NOT NULL,
  `action` tinyint(4) NOT NULL COMMENT '1: like; -1: dislike',
  `language_id` int(11) NOT NULL DEFAULT '17',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`par_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_like_post
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_like_post`;

CREATE TABLE `soc_like_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: like; -1: dislike',
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '17',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_notifications`;

CREATE TABLE `soc_notifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `soc_post_id` int(11) DEFAULT NULL,
  `soc_comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_parent_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_parent_comment`;

CREATE TABLE `soc_parent_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `liked` int(11) NOT NULL,
  `dislike` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '17',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `word_spam` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`),
  KEY `comment_id` (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_post_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_post_category`;

CREATE TABLE `soc_post_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_posts`;

CREATE TABLE `soc_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `hash_tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_comment` int(11) NOT NULL,
  `total_follow` int(11) NOT NULL,
  `status` tinyint(11) NOT NULL COMMENT '-2: private, -1: deleted, 0: public',
  `liked` int(11) NOT NULL,
  `dislike` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '17',
  `editor_choice` tinyint(2) NOT NULL DEFAULT '0',
  `share` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `word_spam` text COLLATE utf8_unicode_ci,
  `sale` tinyint(1) DEFAULT '0',
  `image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `hash_tag` (`hash_tag`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_rank
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_rank`;

CREATE TABLE `soc_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `num` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '17',
  `rank` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_report`;

CREATE TABLE `soc_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL COMMENT 'post or comment',
  `report_type` tinyint(4) NOT NULL COMMENT '1: post; 2: comment; 3: par_comment',
  `reason` text NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '-1: spam; 1: checked',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table soc_user_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_user_category`;

CREATE TABLE `soc_user_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_user_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_user_notifications`;

CREATE TABLE `soc_user_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `soc_notification_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table soc_view_notify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_view_notify`;

CREATE TABLE `soc_view_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `noti_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table soc_word_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `soc_word_image`;

CREATE TABLE `soc_word_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `word_id` int(11) NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table statistical_word
# ------------------------------------------------------------

DROP TABLE IF EXISTS `statistical_word`;

CREATE TABLE `statistical_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table study_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `study_type`;

CREATE TABLE `study_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table sub_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sub_category`;

CREATE TABLE `sub_category` (
  `subCateId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cateId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `subCateName` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `level` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`subCateId`),
  KEY `created_at` (`created_at`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table time
# ------------------------------------------------------------

DROP TABLE IF EXISTS `time`;

CREATE TABLE `time` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `translate` longtext COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vi',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_center
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_center`;

CREATE TABLE `user_center` (
  `center_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `trackId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`center_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_company
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_company`;

CREATE TABLE `user_company` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_expired_note
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_expired_note`;

CREATE TABLE `user_expired_note` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `note` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_history`;

CREATE TABLE `user_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `translate` int(11) NOT NULL,
  `review` int(11) NOT NULL,
  `report` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_receive_code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_receive_code`;

CREATE TABLE `user_receive_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `qr_code` int(11) NOT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `qr_code` (`qr_code`),
  KEY `device` (`device`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_subscribes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_subscribes`;

CREATE TABLE `user_subscribes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `package_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `expiry_time` timestamp NULL DEFAULT NULL,
  `country` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_amount` int(11) DEFAULT NULL,
  `price_currency` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tokenId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `agreement` tinyint(1) NOT NULL,
  `active` int(11) NOT NULL,
  `soc_armorial_id` tinyint(4) NOT NULL DEFAULT '1',
  `soc_rank` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL,
  `lastest_update` timestamp NULL DEFAULT NULL,
  `premium_date` timestamp NULL DEFAULT NULL,
  `premium_expired_date` timestamp NULL DEFAULT NULL,
  `lifetime` tinyint(1) DEFAULT '0',
  `user_introduce` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `provider_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `google_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dict_register` tinyint(4) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '17',
  `virtual` tinyint(1) NOT NULL DEFAULT '0',
  `apple_token` text COLLATE utf8_unicode_ci,
  `apple_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_company` tinyint(4) NOT NULL DEFAULT '0',
  `company_init` datetime DEFAULT NULL,
  `company_active` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `password` (`password`),
  KEY `email` (`email`),
  KEY `tokenId` (`tokenId`),
  KEY `id` (`id`),
  KEY `status` (`status`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table validate_purchase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `validate_purchase`;

CREATE TABLE `validate_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `verified` text,
  `verified_success` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `verified_success` (`verified_success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table version
# ------------------------------------------------------------

DROP TABLE IF EXISTS `version`;

CREATE TABLE `version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: old; 1: new',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table word_mean_english
# ------------------------------------------------------------

DROP TABLE IF EXISTS `word_mean_english`;

CREATE TABLE `word_mean_english` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word_id` varchar(32) NOT NULL,
  `word` varchar(255) NOT NULL,
  `phonetic` varchar(255) NOT NULL,
  `means` text NOT NULL,
  `report` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table words_wrong
# ------------------------------------------------------------

DROP TABLE IF EXISTS `words_wrong`;

CREATE TABLE `words_wrong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word_id` int(11) NOT NULL,
  `word` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dict` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
