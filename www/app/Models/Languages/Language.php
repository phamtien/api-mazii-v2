<?php

namespace App\Models\Languages;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    const ID_DEFAULT = 17;    // vi
    use HasFactory;

    protected $table = 'language';

    public static function getIdViaShort($name){
        $lang = self::whereShort($name)->first();

        return $lang ? $lang->id : self::ID_DEFAULT;
    }
}
