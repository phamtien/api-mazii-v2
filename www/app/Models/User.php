<?php

namespace App\Models;

use App\Models\Auth\Profile;
use App\Models\Provider\Provider;
use App\Ultilities\Date;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Dyrynda\Database\Casts\EfficientUuid;
use Dyrynda\Database\Support\GeneratesUuid;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;
    use GeneratesUuid;

    const ACTIVE = 1;
    const LOGGED = 1;
    const DELETED = -1;

    protected $primaryKey = 'userId';
    // protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'tokenId',
        'active',
        'status',
        'provider',
        'provider_id',
        'google_id',
        'language_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'uuid' => EfficientUuid::class,
    ];

    /**
     * boot function
     */
    public static function boot(){
        parent::boot();
        static::created(function($model){

        });
    }

    /**
     * ======== Relationships =======
     */
    public function profile(){
        return $this->belongsTo(Profile::class, 'userId', 'user_id');
    }

    public function providers(){
        return $this->hasMany(Provider::class, 'user_id', 'userId');
    }

    //========= End Relationship ========

    public function scopeActive($query){
        return $query->where('status', '<>', self::DELETED)->whereActive(self::ACTIVE);
    }

    public function isPremium(){
        if($this->lifetime)
            return true;
        if(is_null($this->premium_expired_date))
            return false;

        return Date::checkCurrentExpired($this->premium_expired_date);
    }

    public function changeStatus(){
        $this->status = !$this->status;
    }

    public function isCompany(){
        return (is_null($this->company_init)) ? false : true;
    }

    public function companyNotPremium(){
        return (is_null($this->company_active)) ? true : false;
    }

    public function setEmailAttribute($email){
        $this->attributes['email'] = preg_replace('/\s+/', '', strtolower($email));
    }

    public function setPasswordAttribute($password){
        $this->attributes['password'] = $password ? md5($password) : '';
    }
}
