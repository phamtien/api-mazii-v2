<?php

if(!function_exists('encrypt_data')) {
    /**
     * Encrypt data response
     */
    function encrypt_data($data){
        $string = (gettype($data) != "string") ? json_encode($data) : $data;
        $ciphering = config('encrypt.method');
        $options = 0;

        $encryption_iv = config('encrypt.init_vector');
        $encrypt_key = config('encrypt.key');

        return openssl_encrypt($string, $ciphering, $encrypt_key, $options, $encryption_iv);
    }
}

if(!function_exists('decrypt_data')) {
    /**
     * Decrypt data response
     */
    function decrypt_data($string){
        $ciphering = config('encrypt.method');
        $options = 0;

        $decryption_iv = config('encrypt.init_vector');
        $decrypt_key = config('encrypt.key');

        $result = openssl_decrypt($string, $ciphering, $decrypt_key, $options, $decryption_iv);
        return (is_null(json_decode($result))) ? $result : json_decode($result);
    }
}

if(!function_exists('generate_token')) {
    /**
     * Generate token
     */
    function generate_token($length = 30){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

        return md5($randomString);
    }
}


