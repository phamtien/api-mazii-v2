<?php
namespace App\Traits;

trait ApiResponse
{
    /**
     * success
     *
     * @param  string $message
     * @param  array|collection $data
     * @param  int $code
     * @return json object
     */
    public function success($message = null, $data = null, $statusCode = 200){
        return response()->json([
            'success' => true,
            'message' => $message,
            'results' => (config('app.env') == 'production' && $data != null) ? encrypt_data($data) : $data
        ], $statusCode);
    }

    /**
     * error
     *
     * @param  string $message
     * @param  int $statusCode
     * @return json object
     */
    public function error($message = 'Unexpected Exception. Try later.', $statusCode = 500){
        return response()->json([
            'success' => false,
            'status'  => $statusCode,
            'message' => $message
        ], $statusCode);
    }

    /**
     * decrypt
     *
     * @param  string $message
     * @param  string $string
     * @param  int $statusCode
     * @return void
     */
    public function decrypt($message, $string, $statusCode = 200){
        return response()->json([
            'success' => true,
            'status'  => $statusCode,
            'message' => $message,
            'results' => decrypt_data($string)
        ], $statusCode);
    }

    /**
     * responseWithToken
     *
     * @param  string $token
     * @return object
     */
    public function responseWithToken($token, $data = null){
        return response()->json([
            'token_type'   => 'Bearer',
            'access_token' => (config('app.env') == 'production') ? encrypt_data($token) : $token,
            'results'      => (config('app.env') == 'production' && $data != null) ? encrypt_data($data) : $data
        ]);
    }
}
