<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Prophecy\Exception\Doubler\MethodNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponse;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        $response = $this->handleException($request, $exception);
        return $response;
    }

    public function handleException($request, Throwable $e){
        if($e instanceof MethodNotFoundException){
            return $this->error('The specified method for the request is invalid', Response::HTTP_METHOD_NOT_ALLOWED);
        }
        if($e instanceof ValidationException){
            return $this->convertExceptionToResponse($e, $request);
        }
        if($e instanceof ModelNotFoundException){
            $model = strtolower(class_basename($e->getModel()));

            return $this->error("Does not exists any {$model} with the specified identificator", Response::HTTP_NOT_FOUND);
        }
        if($e instanceof AuthenticationException){
            return $this->unauthenticated($request, $e);
        }
        if($e instanceof AuthorizationException){
            return $this->error($e->getMessage(), Response::HTTP_FORBIDDEN);
        }
        if($e instanceof NotFoundHttpException){
            return $this->error('The specified URL cannot be found', Response::HTTP_NOT_FOUND);
        }
        if($e instanceof MethodNotAllowedException){
            return $this->error('The specified method for the request is invalid', Response::HTTP_METHOD_NOT_ALLOWED);
        }
        if($e instanceof HttpException){
            return $this->error(trans('exception.validation', ['message' => $e->getMessage()]), Response::HTTP_BAD_REQUEST);
        }
        if($e instanceof TokenMismatchException){
            return redirect()->back()->withInput($request->input());
        }
        if($e instanceof QueryException){
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1451){
                return $this->error('Cannot remove this resource permanently. It is related with any other resouce', Response::HTTP_CONFLICT);
            }
        }
        if(config('app.debug')){
            return $this->error(mb_convert_encoding($e->getMessage(), 'ASCII'), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->error('Unexpected Exception. Try later', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $this->error('Unauthenticated', Response::HTTP_UNAUTHORIZED);
    }
    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();

        return $this->error($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e) {

        });
    }
}
