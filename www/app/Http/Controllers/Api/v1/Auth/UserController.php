<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Events\Auth\Login;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\LoginRequest;
use App\Http\Resources\UserResource;
use App\Repositories\Interfaces\User\UserRepositoryInterface;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    private $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $credential = $request->only('email', 'password');
        $device = $request->device ?? $request->ip();

        $credential['password'] = md5($credential['password']);

        try {
            if(!$this->attempt($credential)){
                return $this->error('Credentials not match', Response::HTTP_UNAUTHORIZED);
            }
            $token = auth()->user()->createToken($device)->plainTextToken;

            return $this->responseWithToken($token, new UserResource(auth()->user()));
        } catch (\Throwable $th) {
            return $this->error($th->getMessage());
        }
    }

    public function findUser($email){
        $user = $this->user->findByEmail($email);
        return $this->success("User", new UserResource($user));
    }

    /**
     * Redirect the user to the Provider authentication page.
     *
     * @param $provider
     * @return JsonResponse
     */
    public function redirectToProvider($provider){
        $validated = $this->validateProvider($provider);
        if(!is_null($validated)){
            return $validated;
        }
        return Socialite::driver($provider)->stateless()->redirect();
    }

    /**
     * Obtain the user information from Provider.
     *
     * @param $provider
     * @return JsonResponse
     */
    public function handleProviderCallback($provider)
    {
        $validated = $this->validateProvider($provider);
        if (!is_null($validated)) {
            return $validated;
        }
        try {
            $socialUser = Socialite::driver($provider)->stateless()->user();
            $user = $this->user->createUserViaSocialNetwork($socialUser, $provider);

            if($user){
                Auth::login($user, true);
                $token = $user->createToken($provider)->plainTextToken;

                event(new Login($user));

                return $this->responseWithToken($token, new UserResource($user));
            }
            return $this->error('Login failed.', Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (ClientException $exception) {
            return $this->error('Invalid credentials provided.', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    public function logout(){
        auth()->user()->currentAccessToken()->delete();

        return $this->success('Logout successful');
    }

    public function revokeAllToken(){
        auth()->user()->tokens()->delete();

        return $this->success('Logout all device successfull');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function me()
    {
        return $this->success("Infomation", new UserResource(auth()->user()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function attempt($credential){
        $user = $this->user->findUserActive($credential);
        if($user){
            Auth::login($user, true);

            // New event while login
            event(new Login($user));
        }
        return $user;
    }

    /**
     * @param $provider
     * @return JsonResponse
     */
    protected function validateProvider($provider)
    {
        if (!in_array($provider, (array)config('auth.socialite.drivers'))) {
            return $this->error('Please login using '. implode('|', (array)config('auth.socialite.drivers')), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
