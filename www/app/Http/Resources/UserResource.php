<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'        => (string) $this->uuid,
            'username'  => (string) $this->username,
            'email'     => (string) $this->email,
            'rank' => (int) $this->soc_rank,
            'premium'   => (bool) $this->isPremium(),
            'premium_expired' => (string) $this->premium_expired_date,
            'lifetime' => (bool) ($this->lifetime),
            'code' => (int) $this->code,
            'app_review' => (bool) $this->app_review,
            'profile' => new ProfileResource($this->profile)
        ];
    }

    public static function originalAttribute($index){
        $attributes = [
            'id'         => 'uuid',
            'username'   => 'username',
            'password'   => 'password',
            'email'      => 'email'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index){
        $attributes = [
            'uuid'      => 'id',
            'username'  => 'username',
            'password'  => 'password',
            'email'     => 'email'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
