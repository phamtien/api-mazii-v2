<?php

namespace App\Providers;

use App\Events\Auth\Login;
use App\Listeners\Auth\ActivePremiumCompany;
use App\Listeners\Auth\CheckDeviceLimited;
use App\Listeners\Auth\CheckUuid;
use App\Listeners\Auth\SendMailLogin;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        Login::class => [
            CheckDeviceLimited::class,
            ActivePremiumCompany::class,
            CheckUuid::class,
            SendMailLogin::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
