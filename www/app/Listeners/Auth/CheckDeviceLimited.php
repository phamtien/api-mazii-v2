<?php

namespace App\Listeners\Auth;

use App\Events\Auth\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CheckDeviceLimited
{
    const LIMITED_DEVICE = 3;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        // Limited login three device
        if($event->user->tokens()->count() >= self::LIMITED_DEVICE){
            $event->user->tokens()->first()->delete();
        }
    }
}
