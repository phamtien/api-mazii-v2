<?php

namespace App\Listeners\Auth;

use App\Events\Auth\Login;
use App\Mail\SendMailLogin as SendMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Laravel\Sanctum\PersonalAccessToken;

class SendMailLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $event->user;
        if(!empty($user->email)){
            $message = [
                'device' => 'New device',
                'name' => $user->username,
                'email' => $user->email,
            ];
            Mail::to($user->email)->send(new SendMail($message));
        }
    }
}
