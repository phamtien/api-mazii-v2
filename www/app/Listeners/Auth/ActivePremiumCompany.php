<?php

namespace App\Listeners\Auth;

use App\Events\Auth\Login;
use App\Ultilities\Date;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ActivePremiumCompany
{
    const PREMIUM_COMPANY = 30;     //day
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $event->user;
        if($user->isCompany() && $user->companyNotPremium()){
            // Check premium
            if(Date::checkCurrentExpired($user->premium_expired_date)){
                $user->premium_expired_date = Carbon::parse($user->premium_expired_date)->addDays(self::PREMIUM_COMPANY)->format('Y-m-d H:i:s');
            }else{
                $user->premium_expired_date = Carbon::now()->addDays(self::PREMIUM_COMPANY)->format('Y-m-d H:i:s');
            }
            $user->company_active = Carbon::now()->format('Y-m-d H:i:s');
            $user->save();
        }
    }
}
