<?php

namespace App\Listeners\Auth;

use App\Events\Auth\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Ramsey\Uuid\Uuid;

class CheckUuid
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Login $event)
    {
        // If uuid null then generate uuid default version 4
        if(is_null($event->user->uuid)){
            try {
                $uuid = call_user_func([Uuid::class, 'uuid4']);
                $event->user->uuid = strtolower($uuid->toString());
            } catch (\Throwable $th) {
                // Throw $th
            }
            $event->user->save();
        }
    }
}
