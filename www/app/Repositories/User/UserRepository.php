<?php

namespace App\Repositories\User;

use App\Models\Languages\Language;
use App\Models\Provider\Provider;
use App\Models\User;
use App\Repositories\BaseRepository;
use App\Repositories\Interfaces\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    const PREMIUM_COMPANY = 30;      // day
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function findUserActive(array $credential)
    {
        $user = $this->model->active()->where($credential)->first();

        return $user;
    }

    public function createUserViaSocialNetwork(\Laravel\Socialite\Contracts\User $socialUser, $provider)
    {
        try {
            $email = $socialUser->getEmail();
            $name  = $socialUser->getName();
            $avatar = $socialUser->getAvatar();

            DB::beginTransaction();
            $localUser = Provider::whereProvider($provider)->whereProviderId($socialUser->getId())->whereHas('user')->with('user')->first(['user_id']);
            if($localUser){
                $user = $localUser->user;
            }else{
                // Check if exists email register
                if(!empty($email) && $localUser = $this->model->whereEmail($email)->first()){
                    $user = $localUser;
                }else{
                    $user = $this->model->create([
                        'email' => $email,
                        'tokenId' => generate_token(),
                        'username' => $name,
                        'status' => true,
                        'active' => true,
                        'provider' => $provider,
                        'provider_id' => ($provider == 'facebook') ? $socialUser->getId() : '',
                        'google_id' => ($provider == 'google') ? $socialUser->getId() : '',
                        'language_id' => Language::getIdViaShort($socialUser->user['locale']),
                    ]);
                    // Create profile after user created
                    $location = \Location::get(request()->ip());
                    $user->profile()->create([
                        'user_id' => $user->userId,
                        'profile_name' => 'user',
                        'name' => $name,
                        'image' => $avatar,
                        'email' => $email,
                        'country' => $location ? $location->countryName : null,
                    ]);
                }
            }
            // Update or Create new provider
            $user->providers()->updateOrCreate([
                'provider' => $provider,
                'provider_id' => $socialUser->getId()
            ],[
                'avatar' => $avatar
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return;
        }
        return $user;
    }

    public function findByEmail(string $email)
    {
        $minutes = 3000;
        $user = Cache::tags('user')->remember("user_by_email_$email", $minutes, function()use($email){
            return $this->model->whereEmail($email)->first();
        });

        return $user;
    }

}
