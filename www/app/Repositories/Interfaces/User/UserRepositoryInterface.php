<?php

namespace App\Repositories\Interfaces\User;

use App\Models\User;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use Laravel\Socialite\Contracts\User as Socialite;

interface UserRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * Find user is active
     *
     * @param  array $credential
     * @return User
     */
    public function findUserActive(array $credential);

    /**
     * createUserViaSocialNetwork
     *
     * @param  mixed $user
     * @param  mixed $provider
     * @param  mixed $providerId
     * @return User
     */
    public function createUserViaSocialNetwork(Socialite $user, $provider);

    /**
     * findByEmail
     *
     * @param  mixed $email
     * @return User
     */
    public function findByEmail(string $email);

    
}
