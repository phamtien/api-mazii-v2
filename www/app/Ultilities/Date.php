<?php

namespace App\Ultilities;

use Carbon\Carbon;

class Date {

    public static function checkCurrentExpired($time){
        $now = Carbon::now()->timestamp;
        $expired = Carbon::parse($time)->timestamp;

        return ($expired <= $now) ? false : true;
    }

    public static function isZeroDate($time){
        if(is_null($time)){
            return true;
        }
        return (strtotime($time) < 0) ? true : false;
    }
}
