@component('mail::message')
    <h3>Xin chào, {{ $data['name'] }}</h3>
    <p>Tài khoản của bạn mới đăng nhập trên thiết bị: {{ $data['device'] }}</p>
    <p>Nếu không phải bạn vui lòng bảo mật tài khoản.</p>
@endcomponent
