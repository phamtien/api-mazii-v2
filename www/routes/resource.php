<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'namespace' => 'Auth'
], function(){
    Route::post('login', 'UserController@login');
    Route::get('login/{provider}', 'UserController@redirectToProvider')->where('provider', implode('|', (array)config('auth.socialite.drivers')));
    Route::get('login/{provider}/callback', 'UserController@handleProviderCallback')->where('provider', implode('|', (array)config('auth.socialite.drivers')));

    Route::group([
        'prefix' => 'users',
        'middleware' => ['auth:sanctum']
    ], function(){
        Route::get('me', 'UserController@me');
        Route::get('{email}', 'UserController@findUser');
        Route::delete('logout', 'UserController@logout');
        Route::delete('revokes', 'UserController@revokeAllToken');
    });
});
