<?php

return [
    'method' => env('ENCRYPT_METHOD'),
    'init_vector' => env('ENCRYPT_INIT_VECTOR'),
    'key' => env('ENCRYPT_KEY'),
];
