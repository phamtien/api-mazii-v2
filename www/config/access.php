<?php

return [
    'rate_limited' => env('API_RATE_LIMIT_ENABLED', 60)
];
