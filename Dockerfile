FROM php:7.3-fpm

WORKDIR /var/www

# Install Composer
ENV COMPOSER_VERSION 2.1.11

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=$COMPOSER_VERSION

# Install nodejs
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        mariadb-client \
        libz-dev \
        libpq-dev \
        libjpeg-dev \
        libpng-dev \
        libssl-dev \
        libzip-dev \
        libonig-dev \
        unzip \
        zip \
        nodejs \
        curl \
    && apt-get clean \
    && docker-php-ext-configure gd \
    && docker-php-ext-configure zip \
    && docker-php-ext-install \
        gd \
        exif \
        opcache \
        pdo_mysql \
        pdo_pgsql \
        pgsql \
        pcntl \
        zip \
        bcmath \
        pdo \
        mbstring \
        mysqli \
        tokenizer \
    && rm -rf /var/lib/apt/lists/*;

# Add user for application
RUN groupadd -g 1000 www && useradd -u 1000 -ms /bin/bash -g www www

# COPY ./www/composer.json .

COPY ./www .

# RUN composer install --no-dev --no-autoloader --no-scripts --ignore-platform-reqs

RUN chown -R www:www .

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000

CMD ["php-fpm"]
